	//Sample code for Hybrid REST Explorer
var selected_contact;
var selcon;
var selected_account;
var selacc;
var selected_opportunity;
var selopp;
var condetails = new Array(10);
var accdetails = new Array(10);
var oppdetails = new Array(10);
function regLinkClickHandlers() {
    var $j = jQuery.noConflict();
	
	forcetkClient.query("SELECT Name,FirstName, LastName,Id, Title, Department, Birthdate, Phone, Email, Signature__c  FROM Contact ORDER BY Name", onSuccessSfdcContacts, onErrorSfdc
	);
	$j.mobile.changePage("#jqm-home",{ transition: "slideup", changeHash: false });
	var logToConsole = cordova.require("salesforce/util/logger").logToConsole;

   /*$j('#').click(function() {
		//logToConsole("link_fetch_device_contacts clicked");
		var contactOptionsType = cordova.require("cordova/plugin/ContactFindOptions");
		var options = new contactOptionsType();
		options.filter = ""; // empty search string returns all contacts
		options.multiple = true;
		var fields = ["name"];
		var contactsObj = cordova.require("cordova/plugin/contacts");
		contactsObj.find(fields, onSuccessDevice, onErrorDevice, options);
	});*/
	
    //fetch values
	$j('#link_fetch_sfdc_contacts,#link_fetch_sfdc_contacts1,#link_fetch_sfdc_contacts2,#link_fetch_sfdc_contacts3,#link_fetch_sfdc_contacts4,#link_fetch_sfdc_contacts5,#link_fetch_sfdc_contacts01,#link_fetch_sfdc_contacts6,#link_fetch_sfdc_contacts7,#link_fetch_sfdc_contacts8,#link_fetch_sfdc_contacts9,#link_fetch_sfdc_contacts0').click(function() {
		//logToConsole("link_fetch_sfdc_contacts clicked");	
		forcetkClient.query("SELECT Name,FirstName, LastName,Id, Title, Department, Birthdate, Phone, Email, Signature__c  FROM Contact ORDER BY Name", onSuccessSfdcContacts, onErrorSfdc);
		$j.mobile.changePage("#jqm-home",{ transition: "slideup", changeHash: false });
	});
    $j('#link_fetch_sfdc_accounts,#link_fetch_sfdc_accounts1,#link_fetch_sfdc_accounts2,#link_fetch_sfdc_accounts3,#link_fetch_sfdc_accounts4,#link_fetch_sfdc_accounts5,#link_fetch_sfdc_accounts6,#link_fetch_sfdc_accounts7,#link_fetch_sfdc_accounts8,#link_fetch_sfdc_accounts9,#link_fetch_sfdc_accounts0,#link_fetch_sfdc_accounts01').click(function() {
		//logToConsole("link_fetch_sfdc_accounts clicked");
		forcetkClient.query("SELECT Name,Id,Type,Industry,Website FROM Account ORDER BY Name", onSuccessSfdcAccounts, onErrorSfdc);
	$j.mobile.changePage("#jqm-account-list",{ transition: "slideup", changeHash: false });		
	});
	$j('#link_fetch_sfdc_opportunities,#link_fetch_sfdc_opportunities1,#link_fetch_sfdc_opportunities2,#link_fetch_sfdc_opportunities3,#link_fetch_sfdc_opportunities4,#link_fetch_sfdc_opportunities5,#link_fetch_sfdc_opportunities6,#link_fetch_sfdc_opportunities7,#link_fetch_sfdc_opportunities8,#link_fetch_sfdc_opportunities8,#link_fetch_sfdc_opportunities9,#link_fetch_sfdc_opportunities0,#link_fetch_sfdc_opportunities01').click(function() {
		//logToConsole("link_fetch_sfdc_opportunity clicked");
		forcetkClient.query("SELECT Name,Id, Type, LeadSource, Amount, CloseDate,StageName, Description  FROM Opportunity ORDER BY Name", onSuccessSfdcOpportunities, onErrorSfdc);
		$j.mobile.changePage("#jqm-opportunity-list",{ transition: "slideup", changeHash: false });
	});
	//view page to create new
	$j('#link_new_sfdc_contact').click(function() {
		$j.mobile.changePage("#jqm-createnewcontact",{ transition: "slideup", changeHash: false });
		clearSignature();
		clearNewContactPage();
	});
	$j('#link_new_sfdc_account').click(function() {
		$j.mobile.changePage("#jqm-createnewaccount",{ transition: "slideup", changeHash: false });
		clearNewAccountPage();
	});
	$j('#link_new_sfdc_opportunity').click(function() {
		$j.mobile.changePage("#jqm-createnewopportunity",{ transition: "slideup", changeHash: false });
		clearNewOpportunityPage();
	});
	//update values
	$j('#link_UpdateContact').click(function(){
	
		clearSignature2();
		$j("#ConUpFirstName").val(condetails.fname);
		$j("#ConUpLastName").val(condetails.lname);
		$j("#ConUpTitle").val(condetails.title);
		$j("#ConUpDepartment").val(condetails.dep);
		$j("#ConUpEmail").val(condetails.email);
		$j("#div_update_sfdc_contact").trigger( "create" )
	});
	$j('#link_UpdateAccount').click(function(){
		$j("#AccUpName").val(accdetails.name);
		$j("#AccUpType").val(accdetails.type);
		$j("#AccUpIndustry").val(accdetails.industry);
		$j("#AccUpWebsite").val(accdetails.website);
		
		$j("#div_update_sfdc_contact").trigger( "create" )
	});
	$j('#link_UpdateOpportunity').click(function(){
		
		$j("#OppUpName").val(oppdetails.name);
		$j("#OppUpType").val(oppdetails.type);
		$j("#OppUpCloseDate").val(oppdetails.closedate);
		$j("#OppUpStageName").val(oppdetails.stagename);
		$j("#OppUpAmount").val(oppdetails.amount);
		$j("#OppUpDescription").val(oppdetails.description);
		
		$j("#div_update_sfdc_contact").trigger( "create" )
	});
	//save new values
	$j('#btnSaveContact').click(function() {
		var data = {};
		var signatura = null;
		signatura = sigCapture.toString();
		
		if($j("#ConlName").val() == ""){
			alert("Last Name must be filled!");		
		}
		else{
			data.FirstName = $j("#ConfName").val();
			data.LastName = $j("#ConlName").val();
			data.Title = $j("#ConTitle").val();
			data.Department = $j("#ConDepartment").val();
			data.Email = $j("#ConEmail").val();
			data.Signature__c = signatura;
			forcetkClient.create("Contact", data, saveDataSuccessContact, saveDataError);
		}
    });	
	$j('#btnSaveAccount').click(function() {
		var data = {};
		
		if($j("#AccName").val() == ""){
			alert("Account Name must be filled!");
		}
		else{
			data.Name = $j("#AccName").val();
			data.Type = $j("#AccType").val();
			data.Industry = $j("#AccIndustry").val();
			data.Website = $j("#AccWebsite").val();
			forcetkClient.create("Account", data, saveDataSuccessAccount, saveDataError);
		}
    });
	$j('#btnSaveOpportunity').click(function() {
		var data = {};
		
		if(($j("#OppName").val() == "") || ($j("#OppStage").val() == "--NONE--" ) || ($j("#OppCloseDate").val() == "")){
			alert("Required filled must be filled!");
		}
		else{
			data.Name = $j("#OppName").val();
			data.Type = $j("#OppType").val();
			data.CloseDate = $j("#OppCloseDate").val();
			data.StageName = $j("#OppStage").val();
			data.LeadSource = $j("#OppLeadSource").val();
			data.Amount = $j("#OppAmount").val();
			data.Description = $j("#OppDescription").val();
			
			forcetkClient.create("Opportunity", data, saveDataSuccessOpportunity, saveDataError);
		}
    });
	//delete values
	$j('#btnDeleteContact').click(function (){
		var data = condetails.Id ;
		var dele = confirm("Are you sure you want to delete this contact?");
		
		if(dele == true ){
			forcetkClient.del("Contact", data);
			alert("Deleting contact successful!");
			$j.mobile.showPageLoadingMsg();
			forcetkClient.query("SELECT Name,FirstName, LastName,Id, Title, Department, Birthdate, Phone, Email  FROM Contact ORDER BY Name", onSuccessSfdcContacts, onErrorSfdc);
			$j.mobile.changePage("#jqm-home",{ transition: "slideup", changeHash: false });
		}
		else{
		}
	});
	$j('#btnDeleteAccount').click(function (){
		var data = accdetails.Id ;
		var dele = confirm("Are you sure you want to delete this account?");
		
		if(dele == true ){
			forcetkClient.del("Account", data);
			alert( "Deleting account successful!");
			$j.mobile.showPageLoadingMsg();
			forcetkClient.query("SELECT Name,Id,Type,Industry,Website FROM Account ORDER BY Name", onSuccessSfdcAccounts, onErrorSfdc);
			$j.mobile.changePage("#jqm-account-list",{ transition: "slideup", changeHash: false });
		}
		else{
		}
	});
	$j('#btnDeleteOpportunity').click(function (){
		var data = oppdetails.Id ;
		var dele = confirm("Are you sure you want to delete this account?");
		
		if(dele == true ){
			forcetkClient.del("Opportunity", data);
			alert("Deleting opportunity successful!");
			$j.mobile.showPageLoadingMsg();
			forcetkClient.query("SELECT Name,Id, Type, LeadSource, Amount, CloseDate, Description  FROM Opportunity ORDER BY Name", onSuccessSfdcOpportunities, onErrorSfdc);
			$j.mobile.changePage("#jqm-opportunity-list",{ transition: "slideup", changeHash: false });
		}
		else{
		}
	});
	//save updated values		
	$j('#btnUpdateContact').click(function(){
		var data = {};
		var aha = condetails.Id;
		var signatura = null;
		signatura = sigCapture2.toString();
		
		if($j("#ConUpLastName").val() == ""){
			alert("Updated Last Name must be filled!");
		}
		else{		
			data.LastName = $j("#ConUpLastName").val();
			data.FirstName = $j("#ConUpFirstName").val();
			data.Title = $j("#ConUpTitle").val();
			data.Department = $j("#ConUpDepartment").val();
			data.Email = $j("#ConUpEmail").val();
			data.Signature__c = signatura;
			forcetkClient.update("Contact", aha, data, updateDataSuccessContact, saveDataErrorko);
		}
	});		
	$j('#btnUpdateAccount').click(function(){
		var data = {};
		var aha = accdetails.Id;
		
		if($j("#AccUpName").val() == ""){
			alert("Account Name must be filled!");
		}
		else{
			data.Name = $j("#AccUpName").val();
			data.Type = $j("#AccUpType").val();
			data.Industry = $j("#AccUpIndustry").val();
			data.Website = $j("#AccUpWebsite").val();
			forcetkClient.update("Account", aha, data, updateDataSuccessAccount, saveDataErrorko);
		}
	});	
	$j('#btnUpdateOpportunity').click(function(){
		var data = {};
		var aha = oppdetails.Id;
		if(($j("#OppUpName").val() == "") || ($j("#OppUpStageName").val() == "--NONE--" ) || ($j("#OppUpCloseDate").val() == "")){
			alert("Required filled must be filled!");
		}
		else{
			data.Name = $j("#OppUpName").val();
			data.Type = $j("#OppUpType").val();
			data.CloseDate = $j("#OppUpCloseDate").val();
			data.StageName = $j("#OppUpStageName").val();
			data.LeadSource = $j("#OppUpLeadSource").val();
			data.Amount = $j("#OppUpAmount").val();
			data.Description = $j("#OppUpDescription").val();
			forcetkClient.update("Opportunity", aha, data, updateDataSuccessOpportunity, saveDataErrorko);
		}
	});
   
   $j('#link_reset').click(function() {
		logToConsole("link_reset clicked");
		$j("#div_device_contact_list").html("")
		$j("#div_sfdc_contact_list").html("")
		$j("#div_sfdc_account_list").html("")
		$j("#console").html("")
	});
    $j('#link_logout,#link_logout1,#link_logout2').click(function() {
		logToConsole("link_logout clicked");
		var sfOAuthPlugin = cordova.require("salesforce/plugin/oauth");
		sfOAuthPlugin.logout();
	});	
}
function onSuccessDevice(contacts) {
    var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessDevice: received " + contacts.length + " contacts");
    $j("#div_device_contact_list").html("");
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_device_contact_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Device Contacts: ' + contacts.length + '</li>'));
    $j.each(contacts, function(i, contact) {
           var formattedName = contact.name.formatted;
           if (formattedName) {
           var newLi = $j("<li><a href='#'>" + (i+1) + " - " + formattedName + "</a></li>");
           ul.append(newLi);
           }
           });
    
    $j("#div_device_contact_list").trigger( "create" )
}
function onErrorDevice(error) {
    cordova.require("salesforce/util/logger").logToConsole("onErrorDevice: " + JSON.stringify(error) );
    alert('Error getting device contacts!');
}
function onSuccessSfdcContacts(response) {
	var $j = jQuery.noConflict();
	selected_contact = new Array(response.totalSize);
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcContacts: received " + response.totalSize + " contacts");
    $j("#div_sfdc_contact_list").html("")
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_sfdc_contact_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Salesforce Contacts: ' + response.totalSize + '</li>'));
	
    $j.each(response.records, function(i, contact) {
           var newLi = $j("<li><a href='#jqm-contact-details'>" + (i+1) + " - " + contact.Name + "</a></li>");
		   selected_contact[i] = contact.Id;
		   newLi.click(function (e){
		   
		   selcon = selected_contact[i];
		   forcetkClient.query("SELECT Name, Title, Department, Birthdate, Phone, Email, Signature__c FROM Contact where Id ='" + selcon + "'", onSuccessContactDetails, onErrorSfdc
		   );
			condetails.Id = contact.Id;
			condetails.fname = contact.FirstName
			condetails.lname = contact.LastName;
			condetails.title = contact.Title;
			condetails.dep = contact.Department;
			condetails.email = contact.Email;
			condetails.sign = contact.Signature__c;
		   });
           ul.append(newLi);
           });
    $j("#div_sfdc_contact_list").trigger( "create" )
}
function onSuccessContactDetails(response){
	var $j = jQuery.noConflict();
	$j("#div_sfdc_contact_details").html("")
	var ul = $j('<ul data-role = "listview" data-inset = "true" data-theme="d" data-divider-theme="b" >');
	$j("#div_sfdc_contact_details").append(ul);
	
	$j.each(response.records, function(i, contact) {
		var con = ($j('<li data-role="list-divider">Name</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Name +'</li>'));
		
		var con = ($j('<li data-role="list-divider">Title</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Title +'</li>'));
		
		var con = ($j('<li data-role="list-divider">Department</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Department +'</li>'));
		
		var con = ($j('<li data-role="list-divider">Birthdate</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Birthdate +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Phone</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Phone +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Email</li>'));
		ul.append(con);
		ul.append($j('<li>' + contact.Email +'</li>'));
		
		//alert(contact.Signature__c);
		var con = ($j('<li data-role="list-divider">Signature</li>'));
		ul.append(con);
		ul.append($j('<li><img src = "' + contact.Signature__c + '" height = "200" width = "500"/></li>'));
	});
	$j("#div_sfdc_contact_details").trigger( "create" )
}
function onSuccessSfdcAccounts(response) {
    selected_account = new Array(response.totalSize);
	var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcAccounts: received " + response.totalSize + " accounts");
    $j("#div_sfdc_account_list").html("")
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_sfdc_account_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Salesforce Accounts: ' + response.totalSize + '</li>'));
    $j.each(response.records, function(i, record) {
           var newLi = $j("<li><a href='#jqm-account-details'>" + (i+1) + " - " + record.Name + "</a></li>");
          
		   selected_account[i] = record.Id;
		   
		   newLi.click(function (e){
		   selacc = selected_account[i];
		   logToConsole(selacc);
		   forcetkClient.query("SELECT Name, Type, Rating, Phone, Website FROM Account where Id ='" + selacc + "'", onSuccessAccountDetails, onErrorSfdc);
			accdetails.Id = record.Id;
			accdetails.name = record.Name;
			accdetails.type = record.Type;
			accdetails.industry = record.Industry;
			accdetails.website = record.Website;
		   });
		   ul.append(newLi);
           });
    $j("#div_sfdc_account_list").trigger( "create" )
}
function onSuccessAccountDetails(response){
	var $j = jQuery.noConflict();
	$j("#div_sfdc_account_details").html("")
	var ul = $j('<ul data-role = "listview" data-inset = "true" data-theme="d" data-divider-theme="b" >');
	$j("#div_sfdc_account_details").append(ul);
	
	$j.each(response.records, function(i, record) {
		var con = ($j('<li data-role="list-divider" >Name</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Name +'</li>'));
		var con = ($j('<li data-role="list-divider" >Type</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Type +'</li>'));
		var con = ($j('<li data-role="list-divider" >Rating</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Rating +'</li>'));
		var con = ($j('<li data-role="list-divider" >Phone</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Phone +'</li>'));
		var con = ($j('<li data-role="list-divider" >Website</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Website +'</li>'));
	});
	$j("#div_sfdc_account_details").trigger( "create" )
}
function onSuccessSfdcOpportunities(response){
	selected_opportunity = new Array(response.totalSize);
	var $j = jQuery.noConflict();
    cordova.require("salesforce/util/logger").logToConsole("onSuccessSfdcAccounts: received " + response.totalSize + " accounts");
    $j("#div_sfdc_opportunity_list").html("")
    var ul = $j('<ul data-role="listview" data-inset="true" data-theme="a" data-dividertheme="a"></ul>');
    $j("#div_sfdc_opportunity_list").append(ul);
    
    ul.append($j('<li data-role="list-divider">Salesforce Opportunities: ' + response.totalSize + '</li>'));
    $j.each(response.records, function(i, record) {
           var newLi = $j("<li><a href='#jqm-opportunity-details'>" + (i+1) + " - " + record.Name + "</a></li>");
		   selected_opportunity[i] = record.Id;
		   newLi.click(function (e){
		   selopp = selected_opportunity[i];
		   logToConsole(selacc);
		   forcetkClient.query("SELECT Name,Id, Type, LeadSource, Amount, CloseDate, StageName, Description  FROM Opportunity where Id ='" + selopp + "'", onSuccessOpportunityDetails, onErrorSfdc);
			oppdetails.Id = record.Id;
			oppdetails.name = record.Name;
			oppdetails.stagename = record.StageName
			oppdetails.type = record.Type;
			oppdetails.leadsource = record.LeadSource;
			oppdetails.amount =record.Amount
			oppdetails.closedate =record.CloseDate
			oppdetails.description =record.Description
			
		   });
		   ul.append(newLi);
           });
    $j("#div_sfdc_opportunity_list").trigger( "create" )
}
function onSuccessOpportunityDetails(response){
	var $j = jQuery.noConflict();
	$j("#div_sfdc_opportunity_details").html("")
	var ul = $j('<ul data-role = "listview" data-inset = "true" data-theme="d" data-divider-theme="b" >');
	$j("#div_sfdc_opportunity_details").append(ul);
	
	$j.each(response.records, function(i, record) {
		var con = ($j('<li data-role="list-divider" >Opportunity Name</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Name +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Type</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Type +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Stage Name</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.StageName +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Lead Source</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.LeadSource +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Amount</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Amount +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Close Date</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.CloseDate +'</li>'));
		
		var con = ($j('<li data-role="list-divider" >Description</li>'));
		ul.append(con);
		ul.append($j('<li>' + record.Description +'</li>'));
	});
	$j("#div_sfdc_opportunity_details").trigger( "create" )
}
function onErrorSfdc( error) {
    //cordova.require("salesforce/util/logger").logToConsole("onErrorSfdc: " + JSON.stringify(error));
	alert(error);
}
//save new values
function saveDataSuccessContact(result){
	alert("Saving new contact successful!");
	var $j = jQuery.noConflict();
	$j.mobile.changePage("#jqm-home",{ transition: "slideup", changeHash: false });
	forcetkClient.query("SELECT Name,FirstName, LastName,Id, Title, Department, Birthdate, Phone, Email  FROM Contact ORDER BY Name", onSuccessSfdcContacts, onErrorSfdc);
}
function saveDataSuccessAccount(result){
	alert("Saving new account successful!");
	var $j = jQuery.noConflict();
	$j.mobile.changePage("#jqm-account-list",{ transition: "slideup", changeHash: false });
	forcetkClient.query("SELECT Name,Id,Type,Industry,Website FROM Account ORDER BY Name", onSuccessSfdcAccounts, onErrorSfdc); 
}
function saveDataSuccessOpportunity(result){
	alert("Saving new opportunity successful!");
	var $j = jQuery.noConflict();
	$j.mobile.changePage("#jqm-opportunity-list",{ transition: "slideup", changeHash: false });
	forcetkClient.query("SELECT Name,Id, Type, LeadSource, Amount, CloseDate, Description  FROM Opportunity ORDER BY Name", onSuccessSfdcOpportunities, onErrorSfdc);
}
//save updated values	
function updateDataSuccessContact(result){
	alert("Updating contact successful!");
	var $j = jQuery.noConflict();
	$j.mobile.showPageLoadingMsg();
	forcetkClient.query("SELECT Name,FirstName, LastName,Id, Title, Department, Birthdate, Phone, Email  FROM Contact ORDER BY Name", onSuccessSfdcContacts, onErrorSfdc);
	$j.mobile.changePage("#jqm-home",{ transition: "slideup", changeHash: false });
}
function updateDataSuccessAccount(result){
	alert("Updating account successful!");
	var $j = jQuery.noConflict();
	$j.mobile.showPageLoadingMsg();
	forcetkClient.query("SELECT Name,Id,Type,Industry,Website FROM Account", onSuccessSfdcAccounts, onErrorSfdc); 
	$j.mobile.changePage("#jqm-account-list",{ transition: "slideup", changeHash: false });
}
function updateDataSuccessOpportunity(result){
	alert("Updating opportunity successful!");
	var $j = jQuery.noConflict();
	$j.mobile.showPageLoadingMsg();
	forcetkClient.query("SELECT Name,Id, Type, LeadSource, Amount, CloseDate, Description  FROM Opportunity ORDER BY Name", onSuccessSfdcOpportunities, onErrorSfdc);
	$j.mobile.changePage("#jqm-opportunity-list",{ transition: "slideup", changeHash: false });
}

function saveDataError(request,error,status){
	alert(request.responseText);
}
function saveDataErrorko(request,error,status){
	alert( request.responseText );
}
function deleteError(request){
	alert( request.responseText );
}

function clearSignature(){
	var canvas = document.getElementById("signature");
	var context = canvas.getContext("2d");
	context.clearRect( 0, 0, canvas.width, canvas.height );
}
function clearSignature2(){
	var canvas = document.getElementById("Updatesignature");
	var context = canvas.getContext("2d");
	context.clearRect( 0, 0, canvas.width, canvas.height );
}

function clearNewContactPage(){
	var $j = jQuery.noConflict();
	$j("#ConfName").val("");
	$j("#ConlName").val("");
	$j("#ConTitle").val("");
	$j("#ConDepartment").val("");
	$j("#ConEmail").val("");
}
function clearNewAccountPage(){
	var $j = jQuery.noConflict();
	$j("#AccName").val("");
	$j("#AccType").val("");
	$j("#AccIndustry").val("");
	$j("#AccWebsite").val("");
}
function clearNewOpportunityPage(){
	var $j = jQuery.noConflict();
	$j("#OppName").val("");
	$j("#OppType").val("-- NONE --");
	$j("#OppCloseDate").val("");
	$j("#OppStage").val("-- NONE --");
	$j("#OppLeadSource").val("-- NONE --");
	$j("#OppAmount").val("");
	$j("#OppDescription").val("");
}